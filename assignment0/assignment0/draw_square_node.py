import rclpy
import time
from rclpy.node import Node
from assignment0.service_call_client import Service_call_client
from geometry_msgs.msg import Twist
from message_pkg.msg import Square
from turtlesim.msg import Pose


class TurtlePubSub(Node):

    def __init__(self):
        super().__init__('TurtlePubSub')
        self.move_pub = self.create_publisher(Twist, '/turtle1/cmd_vel', 1) 
        self.subscription = self.create_subscription(
            Square,
            'cmd_square',
            self.listener_callback,
            1)
        
        self.subMove = self.create_subscription(
            Pose,
            'turtle1/pose',
            self.updatePose,
            1)
        self.client = Service_call_client()
        self.client.send_request_set_pen_off()
        time.sleep(1)
        self.client.send_request_tp()
        time.sleep(1)
        self.client.send_request_set_pen_on()
        time.sleep(1)
        
        self.vel_msg = Twist()
        self.linear_vel = 1
        self.distanceTolorance = 0.1
        self.desired = Pose()
        self.current = Pose()
        self.length = 0
        self.original_length = 0
        self.cw = False


    def turtle_square(self):
        self.length = self.original_length
        self.step = 0
        if (self.cw == False):
            if (self.step == 0):
                self.desired.y = self.current.y - self.length
                if ((self.desired.y) < 1):
                    self.length = self.current.y - 1
                self.step = self.step + 1
                self.publishVel(xAxis=False, positive=False)
                time.sleep(1)
            if (self.step == 1):
                self.desired.x = self.current.x + self.length
                if ((self.desired.x) > 10):
                     self.length = 10 - self.current.x
                self.step = self.step + 1
                self.publishVel(xAxis=True, positive=True)
                time.sleep(1)
            if (self.step == 2):
                self.desired.y = self.current.y + self.length
                if ((self.desired.y) > 10):
                    self.length = 10 - self.current.y
                self.step = self.step + 1
                self.publishVel(xAxis=False, positive=True)
                time.sleep(1)            
            if (self.step == 3):
                self.desired.x = self.current.x - self.length
                if ((self.desired.x) < 1):
                    self.length = self.current.x - 1
                self.step = 0
                self.publishVel(xAxis=True, positive=False)
                time.sleep(1)
        
        else:
            if(self.step == 0):
                self.desired.x = self.current.x + self.length
                if ((self.desired.x) > 10):
                    self.length = 10 - self.current.x
                self.publishVel(xAxis=True, positive=True)
                self.step = self.step + 1
                time.sleep(1)
            if(self.step == 1):
                self.desired.y = self.current.y - self.length
                if ((self.desired.y) < 1):
                    self.length = self.current.y - 1
                self.publishVel(xAxis=False, positive=False)
                self.step = self.step + 1
                time.sleep(1)
            if(self.step == 2):
                self.desired.x = self.current.x - self.length
                if ((self.desired.x) < 1):
                    self.length = self.current.x - 1             
                self.publishVel(xAxis=True, positive=False)
                self.step = self.step + 1   
                time.sleep(1)
            if(self.step == 3):
                self.desired.y = self.current.y + self.length
                if ((self.desired.y) > 10):
                    self.length = 10.0 - self.current.y
                self.publishVel(xAxis=False, positive=True)
                self.step = 0
                time.sleep(1)

            
    
        
    def publishVel(self, xAxis, positive):
        self.setVelocity(xAxis, positive)
        self.move_pub.publish(self.vel_msg)


    def setVelocity(self, xAxis, positive):
        if (xAxis == True and positive == True):
            self.vel_msg.linear.y = float(0)
            self.vel_msg.linear.x = float(self.length)
        elif (xAxis == True and positive == False):
            self.vel_msg.linear.y = float(0)
            self.vel_msg.linear.x = float(-self.length)
        elif (xAxis == False and positive == True):
            self.vel_msg.linear.x = float(0)
            self.vel_msg.linear.y = float(self.length)
        elif (xAxis == False and positive == False):
            self.vel_msg.linear.x = float(0)
            self.vel_msg.linear.y = float(-self.length)

    
    def updatePose(self, msg):
        self.current.x = float(msg.x)
        self.current.y = float(msg.y)
        self.current.theta = msg.theta
           
    def listener_callback(self, msg):
        print(msg)
        self.length = msg.sl
        self.original_length = self.length
        self.cw = msg.cw
        self.turtle_square()

    



def main(args=None):
    rclpy.init(args=args)
    turtle = TurtlePubSub()
    rclpy.spin(turtle)
    rclpy.shutdown()
    

if __name__ == '__main__':
    main()
