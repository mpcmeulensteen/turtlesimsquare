import rclpy
import sys
from rclpy.node import Node
from turtlesim.srv import TeleportAbsolute, SetPen
from assignment0 import service_call_client

class Service_call_tp(Node):
    def __init__(self):
        super().__init__('service_set_pen')
        self.srv_tp = self.create_service(TeleportAbsolute, 'turtle1/teleport_absolute', self.tp_callback)
        self.srv_set_pen = self.create_service(SetPen, 'turtle1/set_pen', self.set_pen_callback)

    def set_pen_callback(self, request: SetPen.Request, response: SetPen.Response):
        sub_request = SetPen.Request()
        sub_request.r = request.r
        sub_request.g = request.g
        sub_request.b = request.b
        sub_request.width = request.width
        sub_request.off = request.off
        return response
    
    def tp_callback(self, request: TeleportAbsolute.Request, response: TeleportAbsolute.Response):
        sub_request = TeleportAbsolute.Request()
        sub_request.x = float(request.x)
        sub_request.y = float(request.y)
        sub_request.theta = float(request.theta)
        return response
        


def main(args=None):
    rclpy.init(args=args)
    service = Service_call_tp()
    rclpy.spin(service)
    
    rclpy.shutdown()
    
        
    

if __name__ == '__main__':
    main()