import sys
import rclpy
from rclpy.node import Node
from turtlesim.srv import TeleportAbsolute, SetPen


class Service_call_client(Node):
    def __init__(self):
        super().__init__('service_call_client')
        self.client_tp = self.create_client(TeleportAbsolute, 'turtle1/teleport_absolute')
        self.client_set_pen = self.create_client(SetPen, 'turtle1/set_pen')
        while not self.client_tp.wait_for_service(timeout_sec=1.0):
            self.get_logger().info('Service not available, waiting again...')
        self.req_tp = TeleportAbsolute.Request()
        while not self.client_set_pen.wait_for_service(timeout_sec=1.0):
            self.get_logger().info('Service not available, waiting again...')
        self.req_set_pen = SetPen.Request()

    def send_request_tp(self):
        self.req_tp.x = 1.0
        self.req_tp.y = 10.0
        self.req_tp.theta = 0.0
        self.future = self.client_tp.call_async(self.req_tp)

    def send_request_set_pen_off(self):
        self.req_set_pen.r = 255
        self.req_set_pen.g = 0
        self.req_set_pen.b = 0
        self.req_set_pen.width = 5
        self.req_set_pen.off = 255
        self.future = self.client_set_pen.call_async(self.req_set_pen)
    
    def send_request_set_pen_on(self):
        self.req_set_pen.r = 255
        self.req_set_pen.g = 0
        self.req_set_pen.b = 0
        self.req_set_pen.width = 5
        self.req_set_pen.off = 0
        self.future = self.client_set_pen.call_async(self.req_set_pen)
    
"""
def main(args=None):
    rclpy.init(args=args)
    
    client = Service_call_client()
    client.send_request_tp()
    
    while rclpy.ok():
        rclpy.spin_once(client)
        if client.future.done():
            try:
                response = client.future.result()
            except Exception as e:
                client.get_logger().info(
                    'Service call failed %r' % (e,))
            break

    client.destroy_node()
    rclpy.shutdown()
    
        
    

if __name__ == '__main__':
    main()
"""