import os
from glob import glob
from setuptools import setup

package_name = 'assignment0'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        (os.path.join('share', package_name), glob('launch/*.launch.py'))
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='student',
    maintainer_email='405582@student.fontys.nl',
    description='TODO: Package description',
    license='BSD',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'draw_square_node = assignment0.draw_square_node:main',
            'draw_square_node_V2 = assignment0.draw_square_node_V2:main',
            'service_call_tp = assignment0.service_call_tp:main'
        ],
    },
)
