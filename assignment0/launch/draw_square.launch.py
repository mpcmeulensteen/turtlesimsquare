from launch import LaunchDescription
from launch_ros.actions import Node


def generate_launch_description():
    ld = LaunchDescription()
    turtle_node = Node(
        package="turtlesim",
        executable="turtlesim_node",
    )
    draw_square_node = Node(
        package="assignment0",
        executable="draw_square_node_V2",
        output="screen"
    )

    ld.add_action(turtle_node)
    ld.add_action(draw_square_node)

    return ld